.. New Copenhagen Notation documentation master file, created by
   sphinx-quickstart on Mon Jan  3 21:00:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to New Copenhagen Notation's documentation!
===================================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Table of Contents

   introduction

   scales/*

   lengths_rhythm/*





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

# Introduction

The *New Copenhagen (Music) Notation* (NCN) is an attempt to improve readablity of music and tries to overcome historic developments that lead to a lot of confusion for the new scholars.
It is tightly connected to the ["scientific music theory"](https://copenhagen-notation.gitlab.io/music-theory/). Many concepts be explained there.


The development of the *New Copenhagen Notation* started 2012 in Copenhagen - that explains the origin of the name. 